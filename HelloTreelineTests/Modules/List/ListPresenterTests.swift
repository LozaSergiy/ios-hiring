//
//  ListPresenterTests.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import XCTest
@testable import HelloTreeline

class ListPresenterTests: XCTestCase {
    
    private var presenter: ListPresenter!
    private var mockRouter: MockRouter!
    private var view: MockViewController!
    
    private let inventoryItems = [
        InventoryItem(id: "test-id-1", title: "title-1", description: "description-1", color: "color-1", available: 1001, cost: 9.91),
        InventoryItem(id: "test-id-2", title: "title-2", description: "description-2", color: "color-2", available: 1002, cost: 9.92),
        InventoryItem(id: "test-id-3", title: "title-3", description: "description-3", color: "color-3", available: 1003, cost: 9.93)
    ]

    override func setUpWithError() throws {
        
        self.mockRouter = MockRouter()
        self.view = MockViewController()
        
        
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = inventoryItems
        
        let soldRepository = DefaultSoldItemsRepository()
        
        let interactor = ListInteractor(itemRepository: itemRepository, sellRepository: soldRepository)
        
        self.presenter = ListPresenter(view: view, interactor: interactor, router: mockRouter)
    }
    
    func testViewWillAppearToLoadListItems() {
        
        // before
        XCTAssertEqual(presenter.itemCount, 0)
        XCTAssertEqual(view.reloadListCalledCount, 0)
        
        // when
        presenter.viewWillAppear()
        
        // then
        XCTAssertEqual(presenter.itemCount, inventoryItems.count)
        XCTAssertEqual(view.reloadListCalledCount, 1)
        for i in 0..<(inventoryItems.count) {
            let listItem = presenter.listItem(at: IndexPath(row: i, section: 0))
            let inventoryItem = inventoryItems[i]
            XCTAssertEqual(listItem.id, inventoryItem.id)
            XCTAssertEqual(listItem.title, inventoryItem.title)
        }
    }
    
    func testListItemSelection() {
        
        // before
        XCTAssertNil(mockRouter.routeToDetailsCallObjectId)
        
        // when
        presenter.viewWillAppear()
        
        let objectId = inventoryItems[0].id
        presenter.listItemSelected(objectId: objectId)
        
        // then
        XCTAssertEqual(mockRouter.routeToDetailsCallObjectId, objectId)
    }
    
    func testReportSending() {
        
        //before
        XCTAssertNil(mockRouter.salesReport)
        
        //when
        let objectId = inventoryItems[0].id
        
        presenter.viewWillAppear()
        presenter.listItemSold(objectId: objectId)
        presenter.onSendReport()
        
        //then
        XCTAssertNotNil(mockRouter.salesReport)
    }
    
    func testOneItemSell() {
        
        //before
        
        let sellItemId = inventoryItems[0].id
        
        presenter.viewWillAppear()

        
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 0)
    
        presenter.listItemSold(objectId: sellItemId)
    
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 1)
    }
    
    func testCancelItemSell() {
        
        //before
        let sellItemId = inventoryItems[0].id
        
        presenter.viewWillAppear()

        
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 0)
    
        presenter.listItemSold(objectId: sellItemId)
    
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 1)

        presenter.listItemSealCancel(objectId: sellItemId)
        
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 0)

    }
    
    func testToManyCancelItemSell() {
        
        //before
        let sellItemId = inventoryItems[0].id
        
        presenter.viewWillAppear()

        
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 0)
    
        presenter.listItemSold(objectId: sellItemId)
    
        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 1)

        presenter.listItemSealCancel(objectId: sellItemId)
        presenter.listItemSealCancel(objectId: sellItemId)

        XCTAssertTrue(presenter.sealCountFor(objectId: sellItemId) == 0)

    }

    func testMultipleItemSell() {
        
        //before
        
        let firstSellItemId = inventoryItems[0].id
        
        presenter.viewWillAppear()
        
        XCTAssertTrue(presenter.sealCountFor(objectId: firstSellItemId) == 0)
    
        presenter.listItemSold(objectId: firstSellItemId)
        presenter.listItemSold(objectId: firstSellItemId)
        presenter.listItemSold(objectId: firstSellItemId)
        presenter.listItemSold(objectId: firstSellItemId)
    
        XCTAssertTrue(presenter.sealCountFor(objectId: firstSellItemId) == 4)
    }

    
}

fileprivate class MockViewController: ListViewController {
    
    var reloadListCalledCount = 0
    func reloadList() {
        reloadListCalledCount += 1
    }
}

fileprivate class MockRouter: ListRouter {
    var salesReport: SalesReport? = nil
    
    func routeToSendReport(_ report: SalesReport) {
        salesReport = report
    }
    
    var routeToDetailsCallObjectId: String? = nil
    func routeToDetails(objectId: String) {
        routeToDetailsCallObjectId = objectId
    }
}
