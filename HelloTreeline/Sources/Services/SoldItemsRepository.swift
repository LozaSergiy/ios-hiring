//
//  SoldItem.swift
//  HelloTreeline
//
//  Created by Sergiy Loza on 30.09.2021.
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol SoldItemsRepository {
    
    func sellItem(_ item: InventoryItem)
    func cancelSell(_ id: String)
    func sellCount(for id: String) -> Int
    func soldItems() -> [SoldItem]
}

class DefaultSoldItemsRepository: SoldItemsRepository {

    static let shared = DefaultSoldItemsRepository()

    private var items:[SoldItem] = []

    func sellCount(for id: String) -> Int {
        return items.first { $0.id == id }?.soldCount ?? 0
    }
    
    func sellItem(_ item: InventoryItem) {
        let info = sealInfo(for: item.id) ?? createInfo(for: item)
        info.increase()
    }
    
    func cancelSell(_ id: String) {
        sealInfo(for: id)?.decrease()
    }
    
    func soldItems() -> [SoldItem] {
        return items
    }
    
    private func sealInfo(for id: String) -> SoldItem? {
        return items.first { $0.id == id }
    }
    
    private func createInfo(for id: InventoryItem) -> SoldItem {
        let newInfo = SoldItem(id: id.id, name: id.title, description: id.description, soldCount: 0)
        items.append(newInfo)
        return newInfo
    }
}
