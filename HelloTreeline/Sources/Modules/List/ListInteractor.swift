//
//  ListInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol ListInteractorDelegate: AnyObject {
    func itemsDidLoad(items: [InventoryItem])
    func sealCountDidChange(for objectId: String)
    func sendReport(_ report: SalesReport)
}

class ListInteractor {
    
    weak var delegate: ListInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository
    private let sellRepository: SoldItemsRepository
    
    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared, sellRepository: DefaultSoldItemsRepository = DefaultSoldItemsRepository.shared) {
        self.itemRepository = itemRepository
        self.sellRepository = sellRepository
    }
    
    func loadItems() {
        itemRepository.getAll { result in
            switch result {
            case .success(let remoteItems):
                self.delegate?.itemsDidLoad(items: remoteItems)
            case .failure(_):
                /// TODO: show error
                break
            }
        }
    }
    
    func sellCount(for id: String) -> Int {
        return sellRepository.sellCount(for: id)
    }
    
    func sellItem(with id: String) {
        guard let inventoryItem = itemRepository.get(objectId: id) else {
            debugPrint("Item with id not found \(id)")
            return
        }
        sellRepository.sellItem(inventoryItem)
        delegate?.sealCountDidChange(for: id)
    }
    
    func cancelItemSell(with id: String) {
        sellRepository.cancelSell(id)
        delegate?.sealCountDidChange(for: id)
    }
    
    func buildReport() {
        
        let message: String = sellRepository.soldItems()
            .filter { $0.soldCount > 0 }
            .map {
                "Item: \($0.name)\n Description: \($0.description)\n Sold: \($0.soldCount)"
            }
            .joined(separator: "\n\n")
        
        let report = SalesReport(subject: "Today Sales Report", recipients: ["bossman@bosscompany.com"], message: message)
        
        delegate?.sendReport(report)
    }
}
