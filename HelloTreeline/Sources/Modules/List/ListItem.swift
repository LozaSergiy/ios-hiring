//
//  ListItem.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

struct ListItem {
    let id: String
    let title: String
    let description: String?
    private(set) var availableCount: Int
    private(set) var sellCount: Int
    
    mutating func set(sellCount to: Int) {
        sellCount = to
    }
}
