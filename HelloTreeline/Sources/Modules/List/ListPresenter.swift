//
//  ListPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class ListPresenter: ListInteractorDelegate {
    
    private let router: ListRouter
    private let interactor: ListInteractor
    private weak var view: ListViewController!
    
    private var listItems: [ListItem] = []
    
    var itemCount: Int { listItems.count }
    
    init(view: ListViewController, interactor: ListInteractor, router: ListRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        self.interactor.delegate = self
    }
    
    func viewWillAppear() {
        interactor.loadItems()
    }
    
    func listItem(at indexPath: IndexPath) -> ListItem {
        listItems[indexPath.row]
    }
    
    func listItemSelected(objectId: String) {
        router.routeToDetails(objectId: objectId)
    }
    
    func listItemSold(objectId: String) {
        interactor.sellItem(with: objectId)
    }
    
    func listItemSealCancel(objectId: String) {
        interactor.cancelItemSell(with: objectId)
    }
    
    func sealCountFor(objectId: String) -> Int {
        return interactor.sellCount(for: objectId)
    }
    
    func onSendReport() {
        interactor.buildReport()
    }
    
    //MARK: - ListInteractorDelegate
    
    func itemsDidLoad(items: [InventoryItem]) {
        self.listItems = items
            .map { listItem(from: $0) }
            .sorted {
                $0.id.caseInsensitiveCompare($1.id) == .orderedAscending
            }
        
        self.view.reloadList()
    }
    
    func sealCountDidChange(for objectId: String) {
        guard let index = listItems.firstIndex (where: { $0.id == objectId }) else {
            return
        }
        listItems[index].set(sellCount: interactor.sellCount(for: objectId))
        self.view.reloadList()
    }
    
    func sendReport(_ report: SalesReport) {
        router.routeToSendReport(report)
    }
    
    
    //MARK: - Private
    
    private func listItem(from item: InventoryItem) -> ListItem {
        let sellCount = interactor.sellCount(for: item.id)
        let listItem: ListItem = .init(id: item.id,
                                       title: item.title,
                                       description: item.description,
                                       availableCount: item.available,
                                       sellCount: sellCount)
        return listItem
    }
}
