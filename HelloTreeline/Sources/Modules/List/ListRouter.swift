//
//  ListRouter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

protocol ListRouter: AnyObject {
    func routeToDetails(objectId: String)
    func routeToSendReport(_ report: SalesReport)
}

class ListDefaultRouter: NSObject, ListRouter {
    
    private weak var viewController: ListDefaultViewController!

    public init(viewController: ListDefaultViewController) {
        self.viewController = viewController
    }

    func routeToDetails(objectId: String) {
        let detailsViewController = DetailsDefaultViewController.build(objectId: objectId)
        viewController.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    func routeToSendReport(_ report: SalesReport) {
        if !MFMailComposeViewController.canSendMail() {
            //TODO: show error to user
            debugPrint("Can't send email on this device")
            return
        }
        let mailController = MFMailComposeViewController()
        mailController.setSubject(report.subject)
        mailController.setToRecipients(report.recipients)
        mailController.setMessageBody(report.message, isHTML: false)
        mailController.mailComposeDelegate = self
        
        viewController.present(mailController, animated: true) {
            
        }
    }
}

extension ListDefaultRouter: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //TODO: handle sending error
        controller.dismiss(animated: true) {
            
        }
    }
}
