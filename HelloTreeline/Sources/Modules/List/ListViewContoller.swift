//
//  ListViewContoller.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

protocol ListViewController: AnyObject {
    func reloadList()
}

class ListDefaultViewController: UIViewController, ListViewController {

    static func build() -> ListDefaultViewController {
        let viewController = UIStoryboard.main.instantiateViewController(of: ListDefaultViewController.self)!
        let router = ListDefaultRouter(viewController: viewController)
        let interactor = ListInteractor()

        viewController.presenter = ListPresenter(view: viewController, interactor: interactor, router: router)

        return viewController
    }

    private var presenter: ListPresenter!

    @IBOutlet weak var tableView: UITableView!
    
    private let CellIdentifier = "ListCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0
        tableView.register(ListTableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        self.navigationController?.setToolbarHidden(false, animated: false)
        self.setToolbarItems([
            UIBarButtonItem.flexibleSpace(),
            UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(onCompose))
        ], animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear()
    }
    
    @objc fileprivate func onCompose() {
        presenter.onSendReport()
    }
    
    func reloadList() {
        tableView.reloadData()
    }
}

extension ListDefaultViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
    }
}

extension ListDefaultViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as? ListTableViewCell else {
            fatalError("cell with \(CellIdentifier) not registered")
        }
        let listItem = presenter.listItem(at: indexPath)
        cell.title = listItem.title
        cell.itemDescription = listItem.description
        cell.itemStatus = "Sold: \(listItem.sellCount)\nAvailable: \(listItem.availableCount - listItem.sellCount)"
        
        cell.minimumStepperValue = 0
        cell.maximumStepperValue = Double(listItem.availableCount)
        cell.currentStepperValue = Double(listItem.sellCount)
        
        cell.onStepperValueChanged = { [weak self] (value) in
            if Int(value) > listItem.sellCount {
                self?.presenter.listItemSold(objectId: listItem.id)
            } else {
                self?.presenter.listItemSealCancel(objectId: listItem.id)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objectId = presenter.listItem(at: indexPath).id
        presenter.listItemSelected(objectId: objectId)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
