//
//  SoldItem.swift
//  HelloTreeline
//
//  Created by Sergiy Loza on 30.09.2021.
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

class ListTableViewCell: UITableViewCell {
    
    private lazy var titleLabel: UILabel = {
        let value = UILabel()
        value.font = UIFont.preferredFont(forTextStyle: .headline)
        contentView.addSubview(value)
        value.translatesAutoresizingMaskIntoConstraints = false
        return value
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let value = UILabel()
        value.font = .preferredFont(forTextStyle: .subheadline)
        value.numberOfLines = 0
        contentView.addSubview(value)
        value.translatesAutoresizingMaskIntoConstraints = false
        return value
    }()
    
    private lazy var statusLabel: UILabel = {
        let value = UILabel()
        value.translatesAutoresizingMaskIntoConstraints = false
        value.numberOfLines = 0
        contentView.addSubview(value)
        return value
    }()
    
    private lazy var salesStepper: UIStepper = {
        let value = UIStepper()
        contentView.addSubview(value)
        value.translatesAutoresizingMaskIntoConstraints = false
        return value
    }()
    
    var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    var itemDescription: String? {
        get { descriptionLabel.text }
        set { descriptionLabel.text = newValue }
    }
    
    var itemStatus: String? {
        get { statusLabel.text }
        set { statusLabel.text = newValue }
    }
    
    var minimumStepperValue: Double {
        get { salesStepper.minimumValue }
        set { salesStepper.minimumValue = newValue }
    }
    
    var maximumStepperValue: Double {
        get { salesStepper.maximumValue }
        set { salesStepper.maximumValue = newValue }
    }
    var currentStepperValue: Double {
        get { salesStepper.value }
        set { salesStepper.value = newValue }
    }
    
    var onStepperValueChanged: ((Double) -> Swift.Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
        salesStepper.addTarget(self, action: #selector(onStepperValueChanged(_:)), for: .valueChanged)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            //Title label constraints
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),

            //Description label constraints
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor , constant: 8),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            
            statusLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 8),
            statusLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            statusLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            statusLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -8),
            
            salesStepper.centerYAnchor.constraint(equalTo: statusLabel.centerYAnchor),
            salesStepper.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8)
        ])
    }
    
    @objc fileprivate func onStepperValueChanged(_ stepper: UIStepper) {
        onStepperValueChanged?(stepper.value)
    }
}
