//
//  SoldItem.swift
//  HelloTreeline
//
//  Created by Sergiy Loza on 30.09.2021.
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class SoldItem {
    
    internal init(id: String, name: String, description: String, soldCount: Int) {
        self.id = id
        self.name = name
        self.description = description
        self.soldCount = soldCount
    }
    
    
    private(set) var id: String
    private(set) var name: String
    private(set) var description: String
    private(set) var soldCount: Int
    
    func increase() {
        soldCount += 1
    }
    
    func decrease() {
        if soldCount == 0 {
            return
        }
        soldCount -= 1
    }
}
