//
//  SoldItem.swift
//  HelloTreeline
//
//  Created by Sergiy Loza on 30.09.2021.
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

struct SalesReport {
    
    let subject: String
    let recipients: [String]
    var message: String
}
